// Golang Program to Add Two Matrix Using Multi-dimensional Arrays
package main

import "fmt"

func main() {

	//=====================================

	for {
		var input int
		fmt.Println("==================")
		fmt.Println("----->MENU<-------")
		fmt.Println(" -----------------")
		fmt.Println("|1. Array 1 Dimensi|")
		fmt.Println(" -----------------")
		fmt.Println("|2. Array 2 Dimensi|")
		fmt.Println(" -----------------")
		fmt.Println("|3. EXIT		  |")
		fmt.Println(" -----------------")
		fmt.Println("==================")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 3 {
			fmt.Println("----------------------------")
			fmt.Println("----- Have A Nice Day ------")
			fmt.Println("----------------------------")
			break
		}

		switch input {
		case 1:
			var array_size int
			fmt.Print("Enter the size of array: ")
			fmt.Scan(&array_size)
			fmt.Print("Enter the array elements: ")

			array := make([]float32, array_size)
			var hasil int
			for i := 0; i < array_size; i++ {
				fmt.Scan(&array[i])
			}
			fmt.Println("Hasil array")
			for i := 0; i < array_size; i++ {
				hasil = int(array[i])
				fmt.Println(hasil)
			}
		case 2:
			var matrix1 [100][100]int
			// var matrix2[100][100] int
			// var sum[100][100] int
			var row, col int
			fmt.Print("Enter number of rows: ")
			fmt.Scan(&row)
			fmt.Print("Enter number of cols: ")
			fmt.Scan(&col)

			fmt.Println()
			fmt.Println("========== Matrix1 =============")
			fmt.Println()
			for i := 0; i < row; i++ {
				for j := 0; j < col; j++ {
					fmt.Printf("Enter the element for Matrix1 %d %d :", i+1, j+1)
					fmt.Scan(&matrix1[i][j])
				}
			}

			// fmt.Println()
			// fmt.Println("========== Matrix2 =============")
			// fmt.Println()

			// for i := 0; i < row; i++ {
			// 	for j := 0; j < col; j++ {
			// 		fmt.Printf("Enter the element for Matrix2 %d %d :",i+1,j+1)
			// 		fmt.Scanln(&matrix2[i][j])
			// 	}
			// }

			// for i := 0; i < row; i++ {
			// 	for j := 0; j < col; j++ {
			// 		sum[i][j] = matrix1[i][j]+matrix2[i][j]
			// 	}
			// }

			fmt.Println()
			fmt.Println("========== Show of Matrix =============")
			fmt.Println()

			for i := 0; i < row; i++ {
				for j := 0; j < col; j++ {
					fmt.Printf(" %d ", matrix1[i][j])
					if j == col-1 {
						fmt.Println("")
					}
				}
			}
		default:
			fmt.Println("Thanks")

		}
	}
}
