package main

import "fmt"

func main() {
    var array_size int
    fmt.Println("Enter the size of array: ")
    fmt.Scan(&array_size)
    fmt.Println("Enter the array elements: ")

    array := make([]float32, array_size)
	var hasil float32
	for i := 0; i < array_size; i++ {
        fmt.Scan(&array[i])
        hasil = getAverage(array)
    }
    fmt.Printf("\nAverage is : %f", hasil)
}

func getAverage(arr[]float32)float32{
	var i int
	var avg,sum float32
	for i = 0 ; i < len(arr); i++{
		sum += arr[i]
	}
	avg = sum/ float32(len(arr))
	return avg
}

			
}