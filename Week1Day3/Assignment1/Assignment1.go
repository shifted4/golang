package main

import (
	"fmt"
	"aldi/shape"
)

func main() {

	// loop that runs infinitely
	for {
		var input int
		fmt.Println("==================")
		fmt.Println("----->MENU<-------")
		fmt.Println(" -----------------")
		fmt.Println("|1. Luas Segitiga |")
		fmt.Println(" -----------------")
		fmt.Println("|2. Luas Lingkaran|")
		fmt.Println(" -----------------")
		fmt.Println("|3. Luas Persegi  |")
		fmt.Println(" -----------------")
		fmt.Println("|4. EXIT		   |")
		fmt.Println(" -----------------")
		fmt.Println("==================")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 4 {
			fmt.Println("----------------------------")
			fmt.Println("----- Have A Nice Day ------")
			fmt.Println("----------------------------")
			break
		}

		switch input {
		case 1:
			fmt.Println("Menghitung Luas Segitiga")
			var alas float32
			var tinggi float32
			// var luas_segitiga float32
			fmt.Print("Masukan alas = ")
			fmt.Scan(&alas)
			fmt.Print("Masukan tinggi = ")
			fmt.Scan(&tinggi)

			 s := shape.Segitiga{
				Alas : alas,
				Tinggi : tinggi,
			}

			fmt.Printf("Luas Segitiga adalah = %.1f\n", s.CountLuas())
		case 2:
			fmt.Println("Menghitung Luas Lingkaran")
			const phi = 3.14
			var jari2 float32
			fmt.Print("Masukan Jari-jari = ")
			fmt.Scan(&jari2)

			l  := shape.Lingkaran{
				Jari2 : jari2,
			}

			fmt.Printf("Luas Lingkaran adalah = %.1f\n", l.CountLuas())
		case 3:
			fmt.Println("Menghitung Luas Persegi")
			var panjang float32
			var lebar float32
			fmt.Print("Masukan panjang = ")
			fmt.Scan(&panjang)
			fmt.Print("Masukan lebar = ")
			fmt.Scan(&lebar)

			p  := shape.Persegi{
				Panjang : panjang,
				Lebar : lebar,
			}

			fmt.Println("Luas Persegi adalah = ", p.CountLuas())
		}

	}
}
