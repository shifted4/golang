package shape

type Segitiga struct {
	Alas   float32
	Tinggi float32
}

func (s Segitiga) CountLuas() float32 {
	return  s.Alas * s.Tinggi / 2
}
