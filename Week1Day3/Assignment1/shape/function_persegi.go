package shape

type Persegi struct {
	Panjang   float32
	Lebar float32
}

func (p Persegi) CountLuas() float32 {
	return  p.Panjang * p.Lebar
}