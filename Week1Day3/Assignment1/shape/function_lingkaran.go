package shape

type Lingkaran struct {
	Jari2  float32
}

func (l Lingkaran) CountLuas() float32 {
	const phi = 3.14
	return  l.Jari2 * l.Jari2 * phi 
}