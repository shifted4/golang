package main

import "fmt"

func main() {
	var k string
	for i := 10; i > 0; i-- {
		for j := 1; j <= i; j++ {
			if j >= i {
				k += "*"
				fmt.Printf(k)
			} else {
				fmt.Printf(" ")
			}
		}
		fmt.Printf("\n")
	}
}
