package main

import (
	"fmt"
	"time"
)

type Casher interface {
	payment() int
}

type Product struct {
	Id    int
	Name  string
	Price int
	Stock int
}

type Transaction struct {
	Id       int
	Products []Product
	Date     string
	Total    int
}

func payment(products *[]Product) Product {
	var inputId int
	var inputQty int
	fmt.Print("Input product id = ")
	fmt.Scan(&inputId)
	fmt.Print("Input quantity = ")
	fmt.Scan(&inputQty)

	var editProduct Product
	for index, product := range *products {
		if product.Id == inputId {
			(*products)[index].Stock = product.Stock - inputQty //pengurangan stock
			editProduct = (*products)[index]
			editProduct.Stock = inputQty
		}
	}
	return editProduct
}

func subTotal(products []Product) int {
	var subTotal int
	var total int
	fmt.Println("No\t Nama\t Qty\t SubTotal")
	for i, el := range products {
		subTotal += int(el.Price) * int(el.Stock)
		fmt.Printf("%d\t %s\t %d\t %d \n", i+1, el.Name, el.Stock, subTotal)
		total += subTotal
	}
	fmt.Println("Total Sementara ", total)
	return total
}

func addTransaction(newProduct []Product, id int, total int) Transaction {
	curentTime := time.Now()
	newTransaction := Transaction{Id: id, Products: newProduct, Date: curentTime.Format(time.ANSIC), Total: total}
	return newTransaction
}

func main() {

	var products []Product
	var itemProducts []Product
	var transactions []Transaction
	var total int

	//dummy product
	products = append(products, Product{Id: 1, Name: "laptop", Price: 1000, Stock: 10})
	products = append(products, Product{Id: 2, Name: "mouse", Price: 20, Stock: 55})
	products = append(products, Product{Id: 3, Name: "keybor", Price: 100, Stock: 67})
	products = append(products, Product{Id: 4, Name: "speaker", Price: 15, Stock: 30})
	products = append(products, Product{Id: 5, Name: "fan", Price: 10, Stock: 20})

	for {
		fmt.Println("=============Exam1===============")
		fmt.Println("1. New Transaction ")
		fmt.Println("2. Transaction Report")
		fmt.Println("3. List Product")
		fmt.Println("0. Exit")
		fmt.Println("=================================")
		var choiceMenu int
		fmt.Print("Choice Menu : ")
		fmt.Scan(&choiceMenu)

		if choiceMenu == 0 {
			fmt.Printf("Program Exited")
			break
		}

		switch choiceMenu {
		case 1:
			for {
				if len(itemProducts) != 0 {
					fmt.Println("Item Transaction")
					total = subTotal(itemProducts)
				}

				//menu transaction
				fmt.Println("-----> Transaction <-----")
				fmt.Println("================================================")
				fmt.Println("1.add more item | 2.see product list | 3.payment")
				fmt.Println("================================================")

				var choiceTransaction int
				fmt.Print("Choice Menu : ")
				fmt.Scan(&choiceTransaction)

				if choiceTransaction == 1 {
					//add more item
					fmt.Println("-----> add more item <-----")
					itemProducts = append(itemProducts, payment(&products))
					// fmt.Println(itemProducts)
				}

				if choiceTransaction == 2 {
					//product list
					fmt.Println("-----> product list <-----")
					fmt.Printf("No.\tName\t\tPrice\tStock\n")
					//looping tampil data product
					for _, item := range products {
						id := item.Id
						name := item.Name
						price := item.Price
						stock := item.Stock
						fmt.Printf("%d\t%s\t\t%d\t%d\n", id, name, price, stock)
					}
					pil := ""
					fmt.Print("Press enter to continue")
					fmt.Scan(&pil)
					if pil == "enter" {break}
				}

				if choiceTransaction == 3 {
					//payment
					fmt.Println("-----> payment <-----")
					//show bill
					fmt.Printf("Bill\t: %d $\n", total)
					var paid int
					fmt.Printf("Paid\t: ")
					fmt.Scan(&paid)

					for {
						if paid < total {
							fmt.Println("Saldo kurang Brow!")
							fmt.Printf("Paid\t: ")
							fmt.Scan(&paid)
						} else {
							break
						}
					}
					//show change
					fmt.Printf("Change\t: %d $\n", paid-total)
					transactions = append(transactions, addTransaction(itemProducts, int(len(transactions))+1, int(total)))
					pil := ""
					fmt.Println("Press enter to continue")
					fmt.Scan(&pil)
					if pil == "enter" {
						break
					}
					break
				}

			}

		case 2:
			for {
				fmt.Println("Transaction Report")
				fmt.Println("ID\t Date\t Total")
				for _, trans := range transactions {
					fmt.Printf("%d\t %s\t %d \n", trans.Id, trans.Date, trans.Total)
				}
				var id int
				fmt.Print("Input ID transaksi: ")
				fmt.Scan(&id)

				for _, trans := range transactions {
					if trans.Id == id {
						subTotal(trans.Products)
						// fmt.Println(trans.Products)
					}
				}
				pil := ""
				fmt.Println("Press enter to continue")
				fmt.Scan(&pil)
				if pil == "enter" {
					break
				}
			}
		case 3:
			//product list
			fmt.Println("-----> product list <-----")
			fmt.Printf("No.\tName\t\tPrice\tStock\n")
			//looping tampil data product
			for _, item := range products {
				id := item.Id
				name := item.Name
				price := item.Price
				stock := item.Stock
				fmt.Printf("%d\t%s\t\t%d\t%d\n", id, name, price, stock)
			}
			fmt.Println("Press enter to continue")
			fmt.Scan()
		default:
			fmt.Println("Pilih Menu yang tertera")
		}
	}
}
