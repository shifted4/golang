package main

import (
	"fmt"
	"time"
)

type Worker struct {
	Id   int
	Nama string
}

type Staff struct {
	Worker
	Jabatan string
}

func inputStaff(sliceStaff *[]Staff) {

	var id int
	fmt.Print("Masukan Id: ")
	fmt.Scan(&id)
	var nama string
	fmt.Print("Masukan Nama: ")
	fmt.Scan(&nama)
	var jabatan string
	fmt.Print("Masukan Jabatan: ")
	fmt.Scan(&jabatan)

	staff := Staff{}
	staff.Id = id
	staff.Nama = nama
	staff.Jabatan = jabatan

	*sliceStaff = append(*sliceStaff, staff)
}

func tampilStaff(staff []Staff, timeInSecs int64) {
	fmt.Println("")
	fmt.Println("--------------------------")
	fmt.Println("| Id | |Nama\t |Jabatan\t|")
	fmt.Println("--------------------------")

	for _, el := range staff {
		fmt.Printf("| %d  |", el.Id)
		fmt.Printf(" |%s\t", el.Nama)
		fmt.Printf(" |%s\t|", el.Jabatan)
		fmt.Println("")
		time.Sleep(time.Duration(timeInSecs) * 1e9)
	}
}

func main() {
	var staff []Staff

	for {
		fmt.Println("=============> MENU <=============")
		fmt.Println("1. Buat Staff ")
		fmt.Println("2. Tampilkan Laporan Staff")
		fmt.Println("99. Exit")
		fmt.Println("=================================")
		var choiceMenu int
		fmt.Print("Choice Menu : ")
		fmt.Scan(&choiceMenu)

		if choiceMenu == 99 {
			fmt.Printf("Program Exited")
			break
		}

		switch choiceMenu {
		case 1:
			inputStaff(&staff)
		case 2:
			fmt.Println("------- Menampilkan Data (Durasi 2 detik) -------")
			go tampilStaff(staff, 2)
		default:
			fmt.Println("pilih sesuai menu!")
		}

	}
}
