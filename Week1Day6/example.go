// package main

// import (
//     "fmt"
//     "time"
// )

// func simulateEvent(name string, timeInSecs int64) { 
//     // sleep for a while to simulate time consumed by event
//     fmt.Println("Started ", name, ": Should take", timeInSecs, "seconds.")
//     time.Sleep(time.Duration(timeInSecs) * 1e9 )
//     fmt.Println("Finished ", name)
// }

// func main() {
//     simulateEvent("100m sprint", 10) //start 100m sprint, it should take 10 seconds
//     simulateEvent("Long jump", 6) //start long jump, it should take 6 seconds
//     simulateEvent("High jump", 3) //start high jump, it should take 3 seconds
// }

package main

import (
    "fmt"
    "strconv"
)

func main() {
    s := "123"

    // string to int
    i, err := strconv.Atoi(s)
    if err != nil {
        // ... handle error
        panic(err)
    }

    fmt.Println(s, i)
}