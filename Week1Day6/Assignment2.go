package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
	// "strings"
)

type Mahasiswa struct {
	Id    int
	Nama  string
	Nilai []int
}

func main() {
	var mhs []Mahasiswa
	for {
		fmt.Println("=============> MENU <=============")
		fmt.Println("1. Create Data Mahasiswa")
		fmt.Println("2. Tampilkan Nilai")
		fmt.Println("3. Tampilkan ke-Layar & Create File")
		fmt.Println("99. Exit")
		fmt.Println("=================================")
		var choiceMenu int
		fmt.Print("Choice Menu : ")
		fmt.Scan(&choiceMenu)

		if choiceMenu == 99 {
			fmt.Printf("Program Exited")
			break
		}

		switch choiceMenu {
		case 1:
			inputMahasiswa(&mhs)
		case 2:
			tampilNilai(&mhs)
		case 3:
			// cs := chan 1
			go tampilMahasiswa(&mhs)
			time.Sleep(time.Duration(3))
			go writeToFile(&mhs)
		}
	}
}

func inputMahasiswa(mahasiswa *[]Mahasiswa) *[]Mahasiswa {

	var id int
	fmt.Print("Masukan Id: ")
	fmt.Scan(&id)
	var nama string
	fmt.Print("Masukan Nama: ")
	fmt.Scan(&nama)

	mhs := Mahasiswa{}
	mhs.Id = id
	mhs.Nama = nama
	var nilai []int
	for i := 1; i <= 3; i++ {
		var n int
		fmt.Printf("Masukan Nilai ke-%d: ", i)
		fmt.Scan(&n)
		nilai = append(nilai, n)
	}
	mhs.Nilai = nilai

	*mahasiswa = append(*mahasiswa, mhs)
	return mahasiswa
}

func tampilNilai(mahasiswa *[]Mahasiswa) {
	for {
		// fmt.Println("Tampil Nilai")
		// fmt.Println("ID\t Date\t Total")
		// for _, trans := range transactions {
		// 	fmt.Printf("%d\t %s\t %d \n", trans.Id, trans.Date, trans.Total)
		// }
		var id int
		fmt.Print("Input ID Mahasiswa: ")
		fmt.Scan(&id)

		for _, mhs1 := range *mahasiswa {
			if mhs1.Id == id {
				fmt.Println("Nilai matematika :", mhs1.Nilai[0])
				fmt.Println("Nilai algoritma :", mhs1.Nilai[1])
				fmt.Println("Nilai machine learning :", mhs1.Nilai[2])
			}
		}
		pil := "ok"
		fmt.Println("Type 'ok' to continue")
		fmt.Scan(&pil)
		if pil == "ok" {
			break
		}
	}
}
func tampilMahasiswa(mahasiswa *[]Mahasiswa) {
	for {
		fmt.Println("Tampil Mahasiswa")
		fmt.Println("ID\t Nama\t Nilai")
		for _, mhs2 := range *mahasiswa {
			fmt.Printf("%d\t %s\t %d \n", mhs2.Id, mhs2.Nama, mhs2.Nilai)
		}
	}
}

func writeToFile(mahasiswa *[]Mahasiswa) {

	f, err := os.Create("data_mahasiswa.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	for _, word := range *mahasiswa {

		id := strconv.Itoa(word.Id)
		nama := word.Nama
		nil1 := strconv.Itoa(word.Nilai[0])
		nil2 := strconv.Itoa(word.Nilai[1])
		nil3 := strconv.Itoa(word.Nilai[2])

		var print string = "|Id : " + id + "|\tNama: " + nama + "|\tNilai: " + nil1 + " , " + nil2 + " dan " + nil3 + "|\n"
		_, err := f.WriteString(print)

		if err != nil {
			log.Fatal(err)
		}
	}
	time.Sleep(time.Duration(3))
	fmt.Println("done")
}
