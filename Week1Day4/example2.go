package main
import "fmt"

type Kitchen struct{
	numOfPlates int
}
type House struct{
	Kitchen //anonymous field (Inheritance)
	numOfRooms int
}

func main(){
	// to initialize you have to use composed type name
	h := House{Kitchen{10}, 3} 
	
	//numOfRooms is a field of House
	fmt.Println("House h has this many rooms : ", h.numOfRooms)
	
	//numOfPlates is a field of anonymous field Kitchen
	//so it can be referred to like a field of House
	fmt.Println("House h has this many plates : ", h.numOfPlates)

	//we can refer to the embedded struct in its entirety by
	//referring to the name of struct type
	fmt.Println("The Kitchen content of this house : ", h.Kitchen)
}