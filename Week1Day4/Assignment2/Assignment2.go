package main

import (
	"bufio"
	"encoding/json"
	"strings"

	// "encoding/json"
	"fmt"
	"os"
	"strconv"
)

func main() {
	// scanner := bufio.NewScanner(os.Stdin)
	var mahasiswa []Mahasiswa
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Input Data Mahasiswa")
		fmt.Println("2. Tampil Data Mahasiswa")
		fmt.Println("3. Seleksi Nilai > 70 = Lulus")
		fmt.Println("4. Exit")
		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}
		switch userInput {
		case 1:
			inputMahasiswa(&mahasiswa)
		case 2:
			for _, el := range mahasiswa {
				fmt.Println(stringify(el))
			}
		case 3:
			for _, el := range mahasiswa {
				if el.Nilai[0] <= 70 {
					fmt.Printf("Nilai Matematika %s adalah %d , Kamu ",el.Nama,el.Nilai[0])
					fmt.Println("Gagal")
				} else {
					fmt.Printf("Nilai Matematika %s adalah %d , Kamu ",el.Nama,el.Nilai[0])
					fmt.Println("Lulus")
				}
				if el.Nilai[1] <= 70 {
					fmt.Printf("Nilai Algoritma %s adalah %d , Kamu ",el.Nama,el.Nilai[1])
					fmt.Println("Gagal")
				} else {
					fmt.Printf("Nilai Algoritma %s adalah %d , Kamu ",el.Nama,el.Nilai[1])
					fmt.Println("Lulus")
				}
				if el.Nilai[2] <= 70 {
					fmt.Printf("Nilai OOP %s adalah %d , Kamu ",el.Nama,el.Nilai[2])
					fmt.Println("Gagal")
				} else {
					fmt.Printf("Nilai OOP %s adalah %d , Kamu ",el.Nama,el.Nilai[2])
					fmt.Println("Lulus")
				}
				if el.Nilai[3] <= 70 {
					fmt.Printf("Nilai Machine Learning %s adalah %d , Kamu ",el.Nama,el.Nilai[3])
					fmt.Println("Gagal")
				} else {
					fmt.Printf("Nilai Machine Learning %s adalah %d , Kamu ",el.Nama,el.Nilai[3])
					fmt.Println("Lulus")
				}
			}
		}
	}
}

type Mahasiswa struct {
	Id      int
	Nama    string
	Jurusan string
	Nilai   []int
}

func inputMahasiswa(mahasiswa *[]Mahasiswa) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Masukan id: ")
	scanner.Scan()
	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan nama: ")
	scanner.Scan()
	nama := scanner.Text()
	fmt.Print("Masukan Jurusan: ")
	scanner.Scan()
	jurusan := scanner.Text()

	//array input
	var arrSize int = 4
	arrNilai := make([]int, arrSize)

	for i := 0; i < arrSize; i++ {
		fmt.Printf("Masukan Nilai ke %d : ", i)
		fmt.Scan(&arrNilai[i])
	}

	mhs := Mahasiswa{}
	mhs.Id = int(id)
	mhs.Nama = nama
	mhs.Jurusan = jurusan
	mhs.Nilai = arrNilai

	// mhs.Toga = mhs.Gapok + mhs.Tunjangan
	*mahasiswa = append(*mahasiswa, mhs)
	printMahasiswa(mhs)
}

func stringify(data interface{}) string {
	b, _ := json.MarshalIndent(data, " ", " ")
	return string(b)
}
func printMahasiswa(mahasiswa Mahasiswa) {
	fmt.Println(stringify(mahasiswa))
}

// func printAllMahasiswa(mahasiswa Mahasiswa){
// 	for mahasiswa allData : Mahasiswa{
// 		fmt.Println(stringify(allData))
// 	}
// }
