package main

import (
	"encoding/json"
	"fmt"
)

func main() {

	//=====================================
	objStaff := Staff{}
	totalTunj := 0
	//=====================================
	
	for {
		var input int
		fmt.Println("==================")
		fmt.Println("----->MENU<-------")
		fmt.Println(" -----------------")
		fmt.Println("|1.Input Data Staff|")
		fmt.Println(" -----------------")
		fmt.Println("|2.Hitung Gaji Staff|")
		fmt.Println(" -----------------")
		fmt.Println("|3.Tampilkan Data Staff|")
		fmt.Println(" -----------------")
		fmt.Println("|4.EXIT			|")
		fmt.Println(" -----------------")
		fmt.Println("==================")
		fmt.Print("Masukan pilihan : ")
		fmt.Scan(&input)
		// condition to terminate the loop
		if input >= 4 {
			fmt.Println("----------------------------")
			fmt.Println("----- Have A Nice Day ------")
			fmt.Println("----------------------------")
			break
		}

		switch input {
		case 1:
			fmt.Println("==> Input Data Staff : <==")
			idStaff := 0
			fmt.Print("Masukan Id : ")
			fmt.Scan(&idStaff)
			namaStaff := ""
			fmt.Print("Masukan Nama : ")
			fmt.Scan(&namaStaff)
			jabatanStaff := ""
			fmt.Print("Masukan Jabatan : ")
			fmt.Scan(&jabatanStaff)
			gapokStaff := 0
			fmt.Print("Masukan Gaji Pokok : ")
			fmt.Scan(&gapokStaff)
			tunjStaff := 0
			fmt.Print("Masukan Tunjangan : ")
			fmt.Scan(&tunjStaff)
			//-----------------------------
			
			objStaff.IdStaff = idStaff
			objStaff.NamaStaff = namaStaff
			objStaff.JabatanStaff = jabatanStaff
			objStaff.GapokStaff = gapokStaff
			objStaff.TunjStaff = tunjStaff
			
			//-----------------------------
		case 2:
			fmt.Println("==> Hitung Gaji Staff : <==")
			fmt.Println("On progress.... please wait....")
			totalTunj = objStaff.GapokStaff + objStaff.TunjStaff
			fmt.Printf("Tunjangan dari %s adalah %d \n\n",objStaff.NamaStaff,totalTunj)
		case 3:
			fmt.Println("==> Tampil Data Staff : <==")
			objStaff.TotalGajiStaff = totalTunj
			showStaff(objStaff)
		}
	}
}

//custroctor 
type Staff struct {
	IdStaff   int    `json:"idStaff,omitempty" validate:"max=10,min=1"`
	NamaStaff string `json:"namaStaff,omitempty"`
	JabatanStaff string `json:"jabatanStaff,omitempty"`
	GapokStaff   int `json:"gapokStaff,omitempty"`
	TunjStaff  int `json:"tunjStaff,omitempty"`
	Status bool
	TotalGajiStaff int `json:"totalGajiStaff,omitempty"`
}

//fuction print book
func showStaff(staff Staff) {
	staff.Status = true
	fmt.Println(stringify(staff))
}
//function to String
func stringify(data interface{}) string {
	b, _ := json.Marshal(data)
	return string(b)
}
