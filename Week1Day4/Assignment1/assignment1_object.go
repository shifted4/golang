package main

import (
	"bufio"
	"encoding/json"
	"strings"

	// "encoding/json"
	"fmt"
	"os"
	"strconv"
)

type Staff struct {
	Id        int
	Nama      string
	Jabatan   string
	Gapok     int
	Tunjangan int
	Toga      int
}

func main() {
	// scanner := bufio.NewScanner(os.Stdin)
	var staff []Staff
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Printf("\n")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tMENU")
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("1. Input Data Staff")
		fmt.Println("2. Hitung Gaji Staff")
		fmt.Println("3. Tampil Data Staff")
		fmt.Println("4. Exit")
		fmt.Printf("Masukan Input: ")
		scanner.Scan()
		userInput, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		if userInput == 4 {
			fmt.Printf("Program Exited")
			break
		}
		switch userInput {
		case 1:
			staff = append(staff, inputStaf())
		case 2:
			staff = hitungGaji(staff)
		case 3:
			for _, el := range staff {
				fmt.Println(stringify(el))
			}
		}
	}
}
func hitungGaji(staff []Staff) []Staff {
	var newStaff []Staff
	for _, el := range staff {
		fmt.Printf("hitung total gaji %s \n", el.Nama)
		el.Toga = el.Gapok + el.Tunjangan
		newStaff = append(newStaff, el)
	}
	return newStaff
}
func inputStaf() Staff {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Masukan id: ")
	scanner.Scan()
	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan nama: ")
	scanner.Scan()
	nama := scanner.Text()
	fmt.Print("Masukan jabatan: ")
	scanner.Scan()
	jabatan := scanner.Text()
	fmt.Print("Masukan gapok: ")
	scanner.Scan()
	gapok, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan tunjangan: ")
	scanner.Scan()
	tunjangan, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	staffNew := Staff{}
	staffNew.Id = int(id)
	staffNew.Nama = nama
	staffNew.Jabatan = jabatan
	staffNew.Gapok = int(gapok)
	staffNew.Tunjangan = int(tunjangan)
	// staffNew.Toga = staffNew.Gapok + staffNew.Tunjangan
	// *staff = append(*staff, staffNew)
	printStaff(staffNew)
	return staffNew
}
func stringify(data interface{}) string {
	b, _ := json.MarshalIndent(data, " ", " ")
	return string(b)
}
func printStaff(staff Staff) {
	fmt.Println(stringify(staff))
}
func hitungToga(staff Staff) {
	fmt.Println(stringify(staff))
}
