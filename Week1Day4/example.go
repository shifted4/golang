package main

import (
	"encoding/json"
	"fmt"
)

type Address struct {
	id       int
	street   string
	building string
}
type Person struct {
	Id      int
	Name    string
	Address Address
}
type Book struct {
	Id      int    `json:"book_id,omitempty" validate:"max=10,min=1"`
	Author  Person `json:"author,omitempty"`
	Subject string `json:"subject,omitempty"`
	Title   string `json:"title,omitempty"`
	Status  bool
}

func main() {
	book3 := Book{}
	var book2 Book
	book3.Title = "Telcom Billing"
	book3.Author.Name = "Toni"
	book3.Id = 12345
	book2 = Book{
		Id:      2345,
		Author:  Person{Name: "Ari"},
		Subject: "Bootcamp",
	}
	printBook(book2)
	printBook(book3)
}

//fuction print book
func printBook(book Book) {
	book.Status = true
	fmt.Println(stringify(book))
}
//function to String
func stringify(data interface{}) string {
	b, _ := json.Marshal(data)
	return string(b)
}