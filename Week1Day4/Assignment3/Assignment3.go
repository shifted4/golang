package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Person interface {
	MyHobby(hobby string) string
}

type Worker struct {
	Id    int64
	Nama  string
	Hobby string
}

func (w Worker) MyHobby(_ string) string {
	return "I'am just a worker"
}

type Staff struct {
	Worker
}

func (s Staff) MyHobby(hobby string) string {
	return hobby
}

type Manager struct {
	Worker
}

func (m Manager) MyHobby(hobby string) string {
	return hobby
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Masukan Id: ")
	scanner.Scan()
	id, _ := strconv.ParseInt(scanner.Text(), 10, 64)
	fmt.Print("Masukan nama: ")
	scanner.Scan()
	nama := scanner.Text()
	fmt.Print("Masukan hobby: ")
	scanner.Scan()
	hobby := scanner.Text()
	fmt.Print("Masukan role: ")
	scanner.Scan()
	role := scanner.Text()

	if role == "staff" {
		s := Staff{Worker{Id: id, Nama: nama, Hobby: hobby}}
		var p Person
		p = Person(s)
		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tData")
		fmt.Println(strings.Repeat("-", 20))

		fmt.Println("ID\t: ", s.Nama)
		fmt.Println("Nama\t: ", s.Nama)
		fmt.Println("Role\t: ", role)
		fmt.Printf("Hallo saya %s, hobby: %s", s.Nama, p.MyHobby(hobby))
	} else if role == "worker" {
		w := Worker{Id: id, Nama: nama, Hobby : hobby}
		var p Person
		p = Person(w)

		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tData")
		fmt.Println(strings.Repeat("-", 20))

		fmt.Println("ID\t: ", w.Nama)
		fmt.Println("Nama\t: ", w.Nama)
		fmt.Println("Role\t: ", role)
		fmt.Printf("Hallo saya %s, hobby: nevermind %s", w.Nama, p.MyHobby(hobby))
	} else if role == "manager" {
		m := Manager{Worker{Id: id, Nama: nama, Hobby: hobby}}
		var p Person
		p = Person(m)

		fmt.Println(strings.Repeat("-", 20))
		fmt.Println("\tData")
		fmt.Println(strings.Repeat("-", 20))

		fmt.Println("ID\t: ", m.Nama)
		fmt.Println("Nama\t: ", m.Nama)
		fmt.Println("Role\t: ", role)
		fmt.Printf("Hallo saya %s, hobby: %s", m.Nama, p.MyHobby(hobby))
	}

}
