package main

import "fmt"

func main() {
	// (:=) is auto detect data type
	nama := "Aldi"
	alamat := "Kebumen, Jateng"
	ttl := "Kebumen, 23 April 2000"
	hoby := "Badminton & Games"
	
	fmt.Println("Nama : " + nama)
	fmt.Println("Alamat : " + alamat)
	fmt.Println("TTL : " + ttl)
	fmt.Println("Hoby : " + hoby)
}
