package main

import "fmt"

func main() {

	a := 5
	b := 2
	c := 3

	tambah := a + a
	kurang := a - a
	kali := a * a
	bagi := a / a
	perhitungan := b + c*a

	fmt.Printf("Penjumlahan : \n")
	fmt.Println(a, " + ", a, " = ", tambah)
	fmt.Printf("Pengurangan : \n")
	fmt.Println(a, " - ", a, " = ", kurang)
	fmt.Printf("Perkalian : \n")
	fmt.Println(a, " * ", a, " = ", kali)
	fmt.Printf("Pembagian : \n")
	fmt.Println(a, " / ", a, " = ", bagi)
	fmt.Printf("Perhitungan : \n")
	fmt.Println(b, " + ", c, " * ", a, " = ", perhitungan)

}
