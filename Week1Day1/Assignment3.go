package main

import "fmt"

func main() {
	// a := 10
	// b := 15

	// tambah := a + b
	// kurang := a - b
	// kali := a * b
	// bagi := a / b

	// fmt.Println("Hasil dari ",a," dan ",b," adalah :")
	// fmt.Println("Penjumlahan : ", tambah)
	// fmt.Println("Pengurangan : ",kurang)
	// fmt.Println("Perkalian : ",kali)
	// fmt.Println("Pembagian : ",bagi)

	// %s = string, %t = tipedata, %f=float
	// var firstName string = "john"

	// var lastName string
	// lastName = "wick"

	// fmt.Printf("halo %s %s!\n", firstName, lastName)

	var a uint8 = 5
	var b uint16 = 6
	var c int8 = -5
	var d int16 = -6
	var e float32 = -5.5
	var f float64 = -6.6
	var g string= "foo"

	fmt.Printf("a = %d is of type %T \n", a, a)
	fmt.Printf("b = %d is of type %T \n", b, b)
	fmt.Printf("c = %d is of type %T \n", c, c)
	fmt.Printf("d = %d is of type %T \n\n", d, d)
	fmt.Printf("e = %.1f is of type %T \n", e, e)
	fmt.Printf("f = %.1f is of type %T \n\n", f, f)
	fmt.Printf("g = %s is of type %T \n", g, g)



}
